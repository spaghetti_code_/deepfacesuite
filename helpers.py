def repeat_to_length(s, wanted): #From https://stackoverflow.com/questions/3391076/repeat-string-to-certain-length
    return (s * (wanted//len(s) + 1))[:wanted]

def prepare_directories():
    import os
    import sys
    try:
        os.mkdir('./data')
    except FileExistsError:
        if not len(os.listdir('./data')) == 0:
            ignore_data = input("There are currently files occupying the data folder. Ignore? (Y/N) : ")
            if not ignore_data.casefold() == 'y'.casefold():
                sys.exit("Data folder contains files.")
        pass
    except:
        raise
    try:
        os.mkdir('./data/media-final')
    except FileExistsError:
        pass
    except:
        raise

def cleanup(insta_username, vsco_username):
    import os
    if not insta_username == "":
        for file in os.listdir(f'./data/{insta_username}'):
            os.rename(f'./data/{insta_username}/{file}', f'./data/media-final/{file}')

    if not vsco_username == "":
        for file in os.listdir(f'./{vsco_username}'):
            os.rename(f'{vsco_username}/{file}', f'./data/media-final/{file}')

    if not insta_username == "" : os.rmdir(f'./data/{insta_username}')
    if not vsco_username == "" : os.rmdir(vsco_username)

""" def get_resolution(path, mode='FULL'):
    from PIL import Image
    im = Image.open(path)
    if mode == 'FULL':
        return im.size
    elif mode == 'HEIGHT' or mode == 'WIDTH':
        width, height = im.size
        if mode == 'HEIGHT':
            return height
        elif mode == 'WIDTH':
            return width
    return im.size

def get_highest_res_image(path, mode):
    import os
    images = {}
    for file in os.listdir(path):
        if os.path.isfile(f'{path}/{file}'):
            filename, file_extension = os.path.splitext(f'{path}/{file}')
            if file_extension == '.png' or file_extension == '.jpg':
                if mode == 'HEIGHT':
                    images[file] = get_resolution(f'{path}/{file}', 'HEIGHT')
                elif mode == 'WIDTH':
                    images[file] = get_resolution(f'{path}/{file}', 'WIDTH')
    images = sorted(images.items(), key=lambda x: x[1])
    name, res = images[-1]
    return name, res

if __name__ == '__main__':
    print('Image with highest width:' )
    name, res = get_highest_res_image('./data/media-final', 'WIDTH')
    print(f'{name} with a width of {res}')
    print('\n')
    name, res = get_highest_res_image('./data/media-final', 'HEIGHT')
    print("Image with highest height:")
    print(f'{name} with a height of {res}') """

def get_file_count(path, prev_max):
    import os
    i = 0
    if not prev_max == None : i = prev_max
    for file in os.listdir(path):
        if os.path.isfile(file) : i += 1
    return i

def combine_folders(path, newpath):
    import os
    directories = []
    for file in os.listdir(path):
        if os.path.isdir(file):
            directories.append(file)
    i = 0
    for directory in directories:
        j = 1
        prev_max = get_file_count(f'{path}/{directory}', prev_max)
        for file in os.listdir(directory):
            filename, file_extension = os.path.splitext(f'{path}/{file}')
            if i > 0:
                os.rename(f'{path}/{file}', f'{newpath}/{prev_max + j}{file_extension}')
            else:
                os.rename(f'{path}/{file}', f'{newpath}/{j}{file_extension}')
            j += 1
        i += 1

def batch_convert_vids(path, newpath):
    import os
    import subprocess
    try:
        os.mkdir(newpath)
    except FileExistsError:
        pass
    except:
        raise
    i = 0
    for file in os.listdir(path):
        if os.path.isfile(f'{path}/{file}'):
            filename, extension = os.path.splitext(f'{path}/{file}')
            if extension == '.mp4':
                #TODO: progress bar using progress pip package, calculated by dividing total frames by current frame
                FNULL = open(os.devnull, 'w')
                process = subprocess.run(f'ffmpeg -i "{path}/{file}" -start_number {i + 1} {newpath}/%05d.png', shell=True)
                print(process.args)
                filename, extension = os.path.splitext(os.listdir(newpath)[-1])
                i = int(filename)
