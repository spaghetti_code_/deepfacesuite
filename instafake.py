import helpers

def get_media():

    import sys
    import subprocess

    helpers.prepare_directories()

    insta_username = input("Instagram Username? : ")
    vsco_username = input("VSCO Username? : ")

    if insta_username == "" and vsco_username == "":
        sys.exit("You must provide an Instagram username, a VSCO username, or both.")

    if not insta_username == "":
        isprivate = input(f'Is {insta_username} on Instagram private? (Y/N) : ')
        if not isprivate.casefold() == 'y'.casefold():
            subprocess.run(['instagram-scraper', insta_username, '-d ./data', '-n'])
            with open('instagram-scraper.log', 'r') as instalog:
                for line in instalog:
                    if f'{insta_username} is private' in line:
                        sys.exit("You said that this Instagram user is not private but they are. Please try again.")
        else:
            insta_login_username = ""
            while insta_login_username == "":
                insta_login_username = input("Username of account that follows this person? : ")
                if insta_login_username == "":
                    print("You must provide a username.")
            insta_login_pass = ""
            while insta_login_pass == "":
                insta_login_pass = input("And password? : ")
                if insta_login_pass == "":
                    print("You must provide a password.")
            
            subprocess.run(['instagram-scraper', insta_username, '-u', insta_login_username, '-p', insta_login_pass, '-d ./data', '-n'])

    if not vsco_username == "":
        subprocess.run(['vsco-scraper', vsco_username, '--getImages'])

    helpers.cleanup(insta_username, vsco_username)

    print("-------")
    print("DONE.")
    print("YOU SHOULD PROBABLY CHECK THE DOWNLOADS AND MANUALLY FILTER OUT ANY MEDIA THAT ISN'T OF THE USERS FACE.")
    print("-------")       


def rename_media():
    import os
    i = 1
    for file in os.listdir('./data/media-final'):
        filename, file_extension = os.path.splitext(f'./data/media-final/{file}')
        numstr = str(i)
        zeroes = helpers.repeat_to_length('0', 5 - len(numstr))
        numstr = zeroes + numstr
        os.rename(f'./data/media-final/{file}', f'./data/media-final/{numstr}{file_extension}')
        i += 1


def main():
    print('''1. Get media
2. Rename media for DeepFaceLab
3. Batch convert videos to frames (optional)''')
    option = input("> ")
    if option == '1':
        get_media()
    elif option == '2':
        rename_media()
    elif option == '3':
        path = input("Folder with videos? : ")
        newpath = input("Output folder? (will be created if it does not exist) : ")
        helpers.batch_convert_vids(path, newpath)

if __name__ == "__main__":
    main()