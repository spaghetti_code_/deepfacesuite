# README #

Instafake is a set of python scripts aiming to make the usage of DeepFaceLab easier, especially with source content from Instagram. Use responsibly.

### Features ###

* Grab media from Instagram using [instagram-scraper](https://github.com/rarcega/instagram-scraper)
* Grab media from VSCO using [vsco-scraper](https://github.com/mvabdi/vsco-scraper)
* Batch convert media to frame images in order to be passed into DeepFaceLab as one video

### Setup ###

* Download DeepFaceLab from [here](https://github.com/iperov/DeepFaceLab)
* `pip install -r requirements.txt`
* `python instafake.py`